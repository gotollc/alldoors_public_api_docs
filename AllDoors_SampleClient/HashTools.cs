﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace AllDoors_SampleClient
{
    public static class HashTools
    {
        public static string GetSha512Hash(string messageToHash)
        {
            var sha = SHA512.Create();

            var hexMessage = Encoding.UTF8.GetBytes(messageToHash);

            var hash = BitConverter.ToString(sha.ComputeHash(hexMessage)).Replace("-", "");

            return hash;
        }
    }
}
