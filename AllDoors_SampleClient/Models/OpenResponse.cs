﻿namespace AllDoors_SampleClient.Models
{
    public class OpenResponse
    {
        public bool Opened { get; set; }

        public string Message { get; set; }
    }
}
