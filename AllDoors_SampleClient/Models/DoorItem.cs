﻿namespace AllDoors_SampleClient.Models
{
    public class DoorItem
    {
        public int DoorId { get; set; }

        public string Name { get; set; }

        public DoorType DoorType { get; set; }

        public string Address { get; set; }

        public float Latitude { get; set; }

        public float Longtitude { get; set; }

        public short TimeZone { get; set; }
    }

    public enum DoorType
    {
        Room,
        Public,
        Parking
    }
}
