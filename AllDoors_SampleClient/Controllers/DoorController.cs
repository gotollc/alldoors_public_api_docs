﻿using AllDoors_SampleClient.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AllDoors_SampleClient.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoorController : ControllerBase
    {
        string Login = "Login";
        string ApiKey = "XXXXXXX";

        [HttpGet("list")]
        public IActionResult List()
        {
            var endpoint = "/door/list";
            var login = Request.Headers["Login"]; //с помощью логина вы можете независимо работать с разными наборами дверей

            var receivedHash = Request.Headers["Hash"];
            var salt = Request.Headers["Salt"];


            var calculatedHash = HashTools.GetSha512Hash(ApiKey + endpoint + salt);

            if (calculatedHash != receivedHash)
            {
                return Unauthorized();
            }

            var doors = new List<DoorItem>
            {
                new DoorItem
                {

                },
                new DoorItem
                {

                }
            };
            return Ok(doors);
        }

        [HttpGet("open")]
        public IActionResult Open(int doorId)
        {
            var endpoint = "/door/open?doorId=" + doorId;

            var login = Request.Headers["Login"]; 

            var receivedHash = Request.Headers["Hash"];
            var salt = Request.Headers["Salt"];

            var calculatedHash = HashTools.GetSha512Hash(ApiKey + endpoint + salt);

            if (calculatedHash != receivedHash)
            {
                return Unauthorized();
            }

            try
            {
                var successResult = new OpenResponse
                {
                    Opened = true,
                    Message = "success"
                };

                return Ok(successResult);
            }
            catch (System.Exception ex)
            {
                var errorResult = new OpenResponse
                {
                    Opened = false,
                    Message = ex.Message
                };

                return Ok(errorResult);
            }
        }
    }
}