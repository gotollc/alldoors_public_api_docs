# Integration of AllDoors solutions with custom ACS

You can configure integration with any ACS, for this it is enough to provide the following callback handlers:

- List all doors
- door opening

URL and authorization data are set in the profile settings.

An example implementation in C# can be found here: https://bitbucket.org/gotollc/alldoors_public_api_docs/src/master/AllDoors_SampleClient/

To test the integration, you must use the sandbox test stand at https://sandbox.alldoors.net/api/ on behalf of the main account which you registered.

To test the operation of keys in the sandbox, you can also install a test mobile application on an Android device and check the interaction of your systems with our API. The application can be downloaded from the link: https://bitbucket.org/gotollc/alldoors_public_api_docs/src/master/AllDoors_android_sandbox.apk

# Authorization

Requests requiring authorization must contain a header

`Authorization` with a value of `Bearer <JWT> `

Where **<JWT>** is the token of the user on whose behalf the action is performed.
The token is issued by the authorization service upon request:

- Address `https://account.alldoors.net/api/account/login`
- The `POST` method
- The expected data type is `application/json`

Request example:

    {
        Login: 'user@example.com',
        Password: 'P@ssw0rd',
    }
    
If the username and password are correct, the object with the token in the `AccessToken` field will be returned.

# Key acquisition

## Get a catalog of all the keys available for purchase

- Connection point `/api/key`
- `GET` method
- The expected data type is `application/json`

Answer example:
     
    [
        {
            "keyId": 1,                           //key identifier required for purchase
            "opensLimit": 10,                     //how many openings this key can do
            "expiration": "2012-12-20T00:00:00",  //key eexpiration date and time
            "price": "50.00",                     //price
            "currency": "USD",                    //currency
            "availableAmount": 5,                 //how many copies of key could be sold
            "durationSeconds": 300000,            //how many seconds will the key be valid after purchase
            "termsOfUse": "tos1",                 //terms of use
            "description": "DEscription1",        //description
            "doors": 
            [
                {
                    "name":"Goto demo door",                   //name of a door
                    "doorId":4844,                             //door id
                    "doorType": "Room",                        //type of door
                    "address": "340 S LEMON AVE #3797",        //address
                    "kycRequired": false,                      //flag indicating that the user must pass KYC
                    "owner": "AT",                             //door owner name (from Ausweis)
                    "ownerPhone": null,                        //owner's phone
                    "latitude": 0,                             //door geolocation coordinates
                    "longtitude": 0,
                    "timeZone": 0                              //UTC shift
                },
                {
                    "name":"Goto demo door", 
                    "doorId":2234,
                    "doorType": "Room",
                    "address": "340 S LEMON AVE #3797",
                    "kycRequired": false,
                    "owner": "AT",
                    "ownerPhone": null,
                    "latitude": 0,
                    "longtitude": 0,
                    "timeZone": 0
                }
            ]
        },
        {
            "keyId": 2,
            "opensLimit": 15,
            "expiration": "2012-12-20T00:00:00",
            "price": "55.50",
            "currency": "USD",
            "availableAmount": 10,
            "doors": 
            [
                {
                    "name":"Goto demo door", 
                    "doorId":2234,
                    "doorType": "Room",
                    "address": "340 S LEMON AVE #3797",
                    "kycRequired": false,
                    "owner": "AT",
                    "ownerPhone": null,
                    "latitude": 0,
                    "longtitude": 0,
                    "timeZone": 0
                }    
            ]
        }
    ]
    

    
    
## Get a list of purchased keys

- Connection point `/api/key/mykeys`
- `GET` method
- The expected data type is `application/json`
- The request requires authorization

The request will return a list of purchased keys of the user identified by the token

Answer example:

    [
        {
            "keyId": 1,                           //key identifier required for purchase
            "opensLimit": 10,                     //how many openings this key can do
            "expiration": "2012-12-20T00:00:00",  //key eexpiration date and time
            "price": "50.00",                     //price
            "currency": "USD",                    //currency
            "availableAmount": 5,                 //how many copies of key could be sold
            "durationSeconds": 300000,            //how many seconds will the key be valid after purchase
            "termsOfUse": "tos1",                 //terms of use
            "description": "DEscription1",        //description
            "doors": 
            [
                {
                    "name":"Goto demo door",                   //name of a door
                    "doorId":4844,                             //door id
                    "doorType": "Room",                        //type of door
                    "address": "340 S LEMON AVE #3797",        //address
                    "kycRequired": false,                      //flag indicating that the user must pass KYC
                    "owner": "AT",                             //door owner name (from Ausweis)
                    "ownerPhone": null,                        //owner's phone
                    "latitude": 0,                             //door geolocation coordinates
                    "longtitude": 0,
                    "timeZone": 0                              //UTC shift
                },
                {
                    "doorType": "Room",
                    "address": "340 S LEMON AVE #3797",
                    "kycRequired": false,
                    "owner": "AT",
                    "ownerPhone": null,
                    "latitude": 0,
                    "longtitude": 0,
                    "timeZone": 0
                }
            ]
        },
        {
            "keyId": 2,
            "purchased": "2020-02-03T16:54:58.649302",
            "opensRemaining": 9,
            "opensLimit": 15,
            "expiration": "2012-12-20T00:00:00",
            "price": "55.50",
            "currency": "USD",
            "availableAmount": 10,
            "doors": 
            [
                {
                    "doorType": "Room",
                    "address": "340 S LEMON AVE #3797",
                    "kycRequired": false,
                    "owner": "AT",
                    "ownerPhone": null,
                    "latitude": 0,
                    "longtitude": 0,
                    "timeZone": 0
                }    
            ]
        }
    ]

    
# Opening the door

To open the door with the purchased key, the following request is executed.
Before opening, the system will check the remaining limits and notify if they are exhausted

- Connection point `/api/door/open?DoorId={id}`
- The `POST` method
- The expected data type is `application/json`
- The request requires authorization

Opens the selected door, if the user has the right to do so.

Answer example:

{
    "opened": true
}

# Selling keys

To sell keys, you first need to bind to your personal account the doors registered with one of the access providers: `Simplegate` or `Ausweis`.
Instructions for your own door control implementation are here: https://bitbucket.org/gotollc/alldoors_public_api_docs/src/master/AllDoors_API_for_ACS_Eng.md

To tie the door to your account you need to specify the account details of the corresponding access provider in your personal account at https://my.alldoors.net


# Door Administration

If the account of the access control provider is added correctly and the doors are tied to it on the provider’s side,
then you can view them with the following query:

- Connection point `/api/door`
- `GET` method
- The request requires authorization

Answer example:

    [
        {
            "doorId": 4849,
            "externalDoorId": 123,
            "deviceId": null,
            "pinId": null,
            "name": "Testing door",
            "doorType": 1,
            "address": "Address here",
            "kycRequired": false,
            "latitude": 1.1,
            "longtitude": 1.1,
            "timeZone": 1,
            "image": null
        },
        {
            "doorId": 4850,
            "externalDoorId": 123,
            "deviceId": null,
            "pinId": null,
            "name": "Real testing door",
            "doorType": 1,
            "address": "Another address",
            "kycRequired": false,
            "latitude": 2.2,
            "longtitude": 2.2,
            "timeZone": 1,
            "image": null
        }
    ]
    
You can also add a door manually (this method is used only if it was not possible to add the door in the regular way described above)

- Connection point `/api/door`
- The `POST` method
- The request requires authorization

Request example:

    {
        "ExternalDoorId": 111,
        "Name": "MyDoor",
        "DoorType": "Room",
        "Address": "Somewhere",
        "Latitude": 1.1,
        "Longtitude": 2.2,
        "DoorProviderCredId": 1 //идентификатор реквизитов провайдера дверей
    }
    
Answer example:

    {
        "doorId": 4852
    }

# Key management

When the doors are added, you can create keys to them.

## Add new key

- Connection point `/api/key`
- The `POST` method
- The request requires authorization

Request example:

    {
      "Description": "newKey",                    //key description
      "OpensLimit": 10,                           //openings limit
      "Expiration": "0001-01-01T00:00:00",        //key validity period
      "DurationSeconds": 500,                     //key validity duration
      "Price": 10.0,                              //key price
      "Currency": "RUB",                          //currency  
      "AvailableAmount": 0,                       //quantity of key copies available for purchase
      "TermsOfUse": "",                           //terms of use
      "DoorsIds":                                 //list of door identifiers that the key can open
          [
            {
                "Id": 4850,                       //identifier of the door that the key can open
                "Main": true                      //a flag indicating whether the opening of this door will be taken into count
            },
            {
                "Id": 4849,
                "Main": false
            }
      	]
    }
    
Answer example:

    {
        "keyId": 4        //ID of the generated key
    }

Please note that the key can open several doors.
And with the `"Main": false` flag, you can specify the doors that will not spend opening limits, for example, to enter the floor or other common areas.

To edit the created key, run the following query:

- Connection point `/api/key/{id}`
- `PUT` method
- The request requires authorization

** id ** - identifier of the edited key

The input model is similar to the creation method (POST)

If successful, an answer will come with an empty body and the code `204`

To delete (if no one has bought the key yet):

## Delete key

- Connection point `/api/key/{id}`
- Method `DELETE`
- The request requires authorization

** id ** - key identifier

You cannot delete a key that has already been rented

If successful, an answer will come with an empty body and the code `204`